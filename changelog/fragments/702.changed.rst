API changes:

- Rename :mod:`gemseo.algos.driver_lib` to :mod:`gemseo.algos.driver_library`.
- Rename :class:`.DriverLib` to :class:`.DriverLibrary`.
- Rename :mod:`gemseo.algos.algo_lib` to :mod:`gemseo.algos.algorithm_library`.
- Rename :class:`.AlgoLib` to :class:`.AlgorithmLibrary`.
- Rename :mod:`gemseo.algos.doe.doe_lib` to :mod:`gemseo.algos.doe.doe_library`.
- Rename :mod:`gemseo.algos.linear_solvers.linear_solver_lib` to :mod:`gemseo.algos.linear_solvers.linear_solver_library`.
- Rename :class:`.LinearSolverLib` to :class:`.LinearSolverLibrary`.
- Rename :mod:`gemseo.algos.opt.opt_lib` to :mod:`gemseo.algos.opt.optimization_library`.
