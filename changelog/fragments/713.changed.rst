API changes:

- Rename :attr:`.HDF5Cache.hdf_node_name` to :attr:`.HDF5Cache.hdf_node_path`.
- ``tolerance`` and ``name`` are the first instantiation arguments of :class:`.HDF5Cache`, for consistency with other caches.
- Rename :attr:`.Factory.classes` to :attr:`.Factory.class_names`.
- Use ``True`` as default value of ``eval_observables`` in :meth:`.OptimizationProblem.evaluate_functions`.
- Rename ``outvars`` to ``output_names`` and ``args`` to ``input_names`` in :class:`.MDOFunction` and its subclasses (names of arguments, attributes and methods).
- :attr:`.MDOFunction.has_jac` is a property.
- Remove :meth:`.MDOFunction.has_dim`.
- Remove :meth:`.MDOFunction.has_outvars`.
- Remove :meth:`.MDOFunction.has_expr`.
- Remove :meth:`.MDOFunction.has_args`.
- Remove :meth:`.MDOFunction.has_f_type`.
- Rename :meth:`.DriverLib.is_algo_requires_grad` to :meth:`.DriverLibrary.requires_gradient`.
- Remove ``n_legend_cols`` in :meth:`.ParametricStatistics.plot_criteria`.
- Rename ``variables_names``, ``variables_sizes`` and ``variables_types`` to ``variable_names``, ``variable_sizes`` and ``variable_types``.
- Rename ``inputs_names`` and ``outputs_names`` to ``input_names`` and ``output_names``.
- Rename ``constraints_names`` to ``constraint_names``.
- Rename ``functions_names`` to ``function_names``.
- Rename ``inputs_sizes`` and ``outputs_sizes`` to ``input_sizes`` and ``output_sizes``.
- Rename ``disciplines_names`` to ``discipline_names``.
- Rename ``jacobians_names`` to ``jacobian_names``.
- Rename ``observables_names`` to ``observable_names``.
- Rename ``columns_names`` to ``column_names``.
- Rename ``distributions_names`` to ``distribution_names``.
- Rename ``options_values`` to ``option_values``.
- Rename ``constraints_values`` to ``constraint_values``.
- Rename ``jacobians_values`` to ``jacobian_values``.
- Rename :class:`.ConstrAggegationDisc` to :class:`.ConstraintAggregation`.
