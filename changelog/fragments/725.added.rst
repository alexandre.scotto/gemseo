:class:`.GradientSensitivity` plots the positive derivatives in red and the negative ones in blue for easy reading.
