API change: :class:`.DOEScenario` no longer has a ``seed`` attribute.
